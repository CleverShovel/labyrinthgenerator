﻿using System.Collections.Generic;

namespace UltraMaze.Algoritms.MazeSolvingAlgorithms
{
    class LeeAlgorithm : PathAlgorithm
    {
        internal struct Cell
        {
            public int X;
            public int Y;

            public Cell(int x, int y)
            {
                X = x;
                Y = y;
            }
        }

        private const int Distance = 1;

        private struct CellString
        {
            public Cell[] Cells;
            public int Size;
        }
        
        private Cell _startCell;
        private Cell _currentCell;
        private Cell _endCell;
        private List<Cell> _currentWave;
        private int _step;

        public override int[,] GeneratePath(int[,] maze)
        {
            Width = maze.GetUpperBound(1) + 1;
            Height = maze.GetUpperBound(0) + 1;
            Maze = new int[Height, Width];

            _startCell = new Cell(1, 1);
            _endCell = new Cell(Width - 2, Height - 2);
            _currentCell = new Cell(-1, -1);
            IsOver = false;

            for (var i = 0; i < Height; i++)
                for (var j = 0; j < Width; j++)
                {
                    Maze[i, j] = (int)((maze[i, j] == (int)MazeEnum.Wall) ? MazeEnum.Wall : MazeEnum.Unvisited);
                }
            Maze[_endCell.Y, _endCell.X] = 1;
            _step = 2;

            _currentWave = new List<Cell> {_endCell};

            IsOver = false;

            return Maze;
        }

        public override int[,] GetNextState()
        {
            if (_currentWave.Count > 0)
            {
                var curWave = _currentWave.ToArray();
                _currentWave = new List<Cell>();
                foreach (var cell in curWave)
                {
                    var nextWave = GetNeighbours(Width, Height, Maze, cell, true);
                    for (var i = 0; i < nextWave.Size; i++)
                    {
                        var neighbourCell = nextWave.Cells[i];
                        _currentWave.Add(neighbourCell);
                        Maze[neighbourCell.Y, neighbourCell.X] = _step;
                    }
                }
                _step++;
            }
            else
            {
                if (_currentCell.Y == -1 && _currentCell.X == -1) _currentCell = _startCell;
                else
                {
                    var neighbours = GetNeighbours(Width, Height, Maze, _currentCell, false);
                    for (var i = 0; i < neighbours.Size; i++)
                    {
                        var neighbourCell = neighbours.Cells[i];
                        if (Maze[neighbourCell.Y, neighbourCell.X] + 1 == Maze[_currentCell.Y, _currentCell.X])
                        {
                            Maze[_currentCell.Y, _currentCell.X] = Maze[_endCell.Y, _endCell.X];
                            _currentCell = neighbourCell;
                        }
                    }
                }
            }

            IsOver = _currentCell.X == _endCell.X && _currentCell.Y == _endCell.Y;

            return Maze;
        }

        private CellString GetNeighbours(int width, int height, int[,] maze, Cell c, bool b)
        {
            var x = c.X;
            var y = c.Y;
            var up = new Cell(x, y - Distance);
            var rt = new Cell(x + Distance, y);
            var dw = new Cell(x, y + Distance);
            var lt = new Cell(x - Distance, y);
            var d = new[] { up, rt, dw, lt };
            var size = 0;

            CellString cells;
            cells.Cells = new Cell[4];

            for (var i = 0; i < 4; i++)
            {
                //для каждого направления
                if (d[i].X > 0 && d[i].X < width - 1 && d[i].Y > 0 && d[i].Y < height - 1)
                {
                    //если не выходит за границы лабиринта
                    var mazeCellCurrent = maze[d[i].Y, d[i].X];
                    var cellCurrent = d[i];
                    if ((!b || mazeCellCurrent != (int) MazeEnum.Unvisited) &&
                        (b || mazeCellCurrent == (int) MazeEnum.Wall)) continue;
                    cells.Cells[size] = cellCurrent;
                    size++;
                }
            }
            cells.Size = size;
            return cells;
        }
    }
}
