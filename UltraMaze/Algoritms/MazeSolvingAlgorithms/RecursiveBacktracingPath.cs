﻿using System.Collections.Generic;

namespace UltraMaze.Algoritms.MazeSolvingAlgorithms
{
    class RecursiveBacktrackingPath : PathAlgorithm
    {
        internal struct Cell
        {
            public int X;
            public int Y;

            public Cell(int x, int y)
            {
                X = x;
                Y = y;
            }
        }

        private const int Distance = 1;

        private struct CellString
        {
            public Cell[] Cells;
            public int Size;
        }

        private readonly Stack<Cell> _stack = new Stack<Cell>();
        private Cell _currentCell;
        private Cell _endCell;

        public override int[,] GeneratePath(int[,] maze)
        {
            Width = maze.GetUpperBound(1) + 1;
            Height = maze.GetUpperBound(0) + 1;

            Maze = new int[Height,Width];
            for (var i = 0; i < Height; i++)
                for (var j = 0; j < Width; j++)
                    Maze[i, j] = (int) ((maze[i, j] == (int) MazeEnum.Wall) ? MazeEnum.Wall : MazeEnum.Unvisited);

            var startCell = new Cell(1, 1);
            _currentCell = startCell;
            Maze[startCell.Y, startCell.X] = (int)MazeEnum.Visited;
            _stack.Push(_currentCell);
            _endCell = new Cell(Width - 2, Height - 2);
            IsOver = false;

            return Maze;
        }

        public override int[,] GetNextState()
        {
            if (IsOver) return Maze;
            var neighbours = GetNeighbours(Width, Height, Maze, _currentCell);
            if (neighbours.Size > 0)
            {
                //если у клетки есть непосещенные соседи
                var randNum = Randomizer.Next(0, neighbours.Size);
                var neighbourCell = neighbours.Cells[randNum];
                _stack.Push(_currentCell); //заносим текущую точку в стек

                _currentCell = neighbourCell; //делаем соседнюю точку текущей и отмечаем ее посещенной
                Maze[_currentCell.Y, _currentCell.X] = (int) MazeEnum.Visited;
            }
            else if (_stack.Count > 0)
            {
                Maze[_currentCell.Y, _currentCell.X] = (int) MazeEnum.Visited + 1;
                //если нет соседей, возвращаемся на предыдущую точку
                _currentCell = _stack.Pop();
            }

            IsOver = (_currentCell.X == _endCell.X && _currentCell.Y == _endCell.Y);

            return Maze;
        }

        private CellString GetNeighbours(int width, int height, int[,] maze, Cell c)
        {
            var x = c.X;
            var y = c.Y;
            var up = new Cell(x, y - Distance);
            var rt = new Cell(x + Distance, y);
            var dw = new Cell(x, y + Distance);
            var lt = new Cell(x - Distance, y);
            var d = new[] { up, rt, dw, lt };
            var size = 0;

            CellString cells;
            cells.Cells = new Cell[4];

            for (var i = 0; i < 4; i++)
            {
                //для каждого направления
                if (d[i].X > 0 && d[i].X < width - 1 && d[i].Y > 0 && d[i].Y < height - 1)
                {
                    //если не выходит за границы лабиринта
                    var mazeCellCurrent = maze[d[i].Y, d[i].X];
                    var cellCurrent = d[i];
                    if (mazeCellCurrent == (int)MazeEnum.Unvisited)
                    {
                        cells.Cells[size] = cellCurrent;
                        size++;
                    }
                }
            }
            cells.Size = size;
            return cells;
        }
    }
}
