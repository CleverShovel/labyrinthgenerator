﻿using System;

namespace UltraMaze.Algoritms.MazeSolvingAlgorithms
{
    internal abstract class PathAlgorithm
    {
        public enum MazeEnum : short
        {
            Wall = -2,
            Cell = 0,
            Visited = 1,
            Unvisited = -1
        };

        protected static int Width;
        protected static int Height;
        protected static int[,] Maze;
        protected static Random Randomizer = new Random();

        internal bool IsOver = false;

        public abstract int[,] GeneratePath(int[,] maze);
        public abstract int[,] GetNextState();
    }

    internal struct Cell
    {
        public int X;
        public int Y;

        public Cell(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}