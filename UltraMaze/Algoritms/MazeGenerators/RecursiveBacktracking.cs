﻿using System;
using System.Collections.Generic;

namespace UltraMaze.Algoritms.MazeGenerators
{
    class RecursiveBacktracking: MazeAlgorithm
    {
        private const int Distance = 2;

        private struct CellString
        {
            public Cell[] Cells;
            public int Size;
        }

        private readonly Stack<Cell> _stack = new Stack<Cell>();
        private int _unvisitedCount;
        private Cell _currentCell;

        public override int[,] GenerateMaze(int width, int height)
        {
            Width = width;
            Height = height;

            Maze = new int[Width, Height];

            for (var i = 0; i < Height; i++)
                for (var j = 0; j < Width; j++)
                    if ((i % 2 != 0 && j % 2 != 0) && (i < Width - 1 && j < Width - 1))
                        Maze[i, j] = (int)MazeEnum.Cell;
                    else Maze[i, j] = (int)MazeEnum.Wall;
            _unvisitedCount = ((Width - 1)/2)*((Height - 1)/2)-1;
            var startCell = new Cell(1, 1);
            _currentCell = startCell;
            Maze[_currentCell.Y, _currentCell.X] = (int)MazeEnum.Visited;

            _stack.Push(startCell);

            return Maze;
        }

        public override int[,] GetNextState()
        {
            var neighbours = GetNeighbours(Width, Height, Maze, _currentCell);
            int randNum;
            if (neighbours.Size != 0)
            {
                //если у клетки есть непосещенные соседи
                randNum = Randomizer.Next(0, neighbours.Size);
                var neighbourCell = neighbours.Cells[randNum];
                _stack.Push(_currentCell); //заносим текущую точку в стек
                Maze = RemoveWall(_currentCell, neighbourCell, Maze);
                //убираем стену между текущей и соседней точками
                _currentCell = neighbourCell; //делаем соседнюю точку текущей и отмечаем ее посещенной
                Maze[_currentCell.Y, _currentCell.X] = (int)MazeEnum.Visited;
                _unvisitedCount--;
            }
            else if (_stack.Count > 0)
            {
                //если нет соседей, возвращаемся на предыдущую точку
                _currentCell = _stack.Pop();
            }
            else
            {
                //если нет соседей и точек в стеке, но не все точки посещены, выбираем случайную из непосещенных
                var cellStringUnvisited = GetUnvisitedCells(Width, Height, Maze, _unvisitedCount);
                randNum = Randomizer.Next(0, cellStringUnvisited.Size);
                _currentCell = cellStringUnvisited.Cells[randNum];
            }

            if (_unvisitedCount == 0) IsOver = true;

            return Maze;
        }

        private CellString GetUnvisitedCells(int width, int height, int[,] maze, int unvisitedCount)
        {
            var unvisitedCells = new CellString { Cells = new Cell[unvisitedCount] };
            var size = 0;
            for (var i = 0; i < height; i++)
                for (var j = 0; j < width; j++)
                    if (maze[i, j] == (int)MazeEnum.Cell)
                    {
                        unvisitedCells.Cells[size] = new Cell(j, i);
                        size++;
                    }
            return unvisitedCells;
        }

        private CellString GetNeighbours(int width, int height, int[,] maze, Cell c)
        {
            var x = c.X;
            var y = c.Y;
            var up = new Cell(x, y - Distance);
            var rt = new Cell(x + Distance, y);
            var dw = new Cell(x, y + Distance);
            var lt = new Cell(x - Distance, y);
            var d = new[] { up, rt, dw, lt };
            var size = 0;

            CellString cells;
            cells.Cells = new Cell[4];

            for (var i = 0; i < 4; i++)
            {
                //для каждого направления
                if (d[i].X > 0 && d[i].X < width - 1 && d[i].Y > 0 && d[i].Y < height - 1)
                {
                    //если не выходит за границы лабиринта
                    var mazeCellCurrent = maze[d[i].Y, d[i].X];
                    var cellCurrent = d[i];
                    if (mazeCellCurrent == (int)MazeEnum.Cell)
                    {
                        cells.Cells[size] = cellCurrent;
                        size++;
                    }
                }
            }
            cells.Size = size;
            return cells;
        }

        private int[,] RemoveWall(Cell first, Cell second, int[,] maze)
        {
            var xDiff = second.X - first.X;
            var yDiff = second.Y - first.Y;
            Cell target;

            var addX = (xDiff != 0) ? (xDiff / Math.Abs(xDiff)) : 0;
            var addY = (yDiff != 0) ? (yDiff / Math.Abs(yDiff)) : 0;

            target.X = first.X + addX; //координаты стенки
            target.Y = first.Y + addY;

            maze[target.Y, target.X] = (int)MazeEnum.Visited;
            return maze;
        }
    }
}
