﻿using System.Collections.Generic;

namespace UltraMaze.Algoritms.MazeGenerators
{
    class KruskalsMazeAlgorithm: MazeAlgorithm
    {
        public class DisjointSet
        {
            private readonly int[] _set;
            private readonly int[] _sizes;
            public int Size;
            public DisjointSet(int size)
            {
                _set = new int[size];
                for (var i = 0; i < size; i++) { _set[i] = i; }
                _sizes = new int[size];
                for (var i = 0; i < size; i++) { _sizes[i] = 1; }
                Size = size;
            }
            public int Find(int item)
            {
                var root = item;
                // find the root
                while (_set[root] != root)
                {
                    root = _set[root];
                }
                // now shorten the paths
                var curr = item;
                while (_set[curr] != root)
                {
                    _set[curr] = root;
                }
                return root;
            }

            public int Join(int item1, int item2)
            {
                var group1 = Find(item1);
                var group2 = Find(item2);
                --Size;
                if (_sizes[group1] > _sizes[group2])
                {
                    _set[group2] = group1;
                    _sizes[group1] += _sizes[group2];
                    return group1;
                }
                _set[group1] = group2;
                _sizes[group2] += _sizes[group1];
                return group2;
            }
        }

        private List<Cell> _walls;
        private DisjointSet _disjoint;

        public override int[,] GenerateMaze(int width, int height)
        {
            Height = height;
            Width = width;
            Maze = new int[Height, Width];
            _walls = new List<Cell>();

            for (var i = 0; i < height; i++)
                for (var j = 0; j < width; j++)
                    if ((i%2 != 0 && j%2 != 0) && (i < height - 1 && j < width - 1))
                        Maze[i, j] = (int) MazeEnum.Cell;
                    else
                    {
                        Maze[i, j] = (int) MazeEnum.Wall;
                        if (i > 0 && i < height - 1 && j > 0 && j < width - 1) _walls.Add(new Cell(j, i));
                    }

            var rows = (height - 1)/2;
            var columns = (width - 1)/2;

            _disjoint = new DisjointSet(rows*columns);

            return Maze;
        }

        public override int[,] GetNextState()
        {
            var wallIndex = Randomizer.Next(_walls.Count);
            var wall = _walls[wallIndex];
            var cell1 = 0;
            var cell2 = 0;
            if (wall.X % 2 != 0)
            {
                cell1 = ((wall.Y - 2)/2)*((Width - 1)/2) + (wall.X - 1)/2;
                cell2 = (wall.Y/2)*((Width - 1)/2) + (wall.X - 1)/2;
            }
            if (wall.Y % 2 != 0)
            {
                cell1 = ((wall.Y - 1)/2)*((Width - 1)/2) + (wall.X - 2)/2;
                cell2 = ((wall.Y - 1)/2)*((Width - 1)/2) + wall.X/2;
            }
            if (_disjoint.Find(cell1) != _disjoint.Find(cell2))
            {
                // we can remove the wall
                Maze[wall.Y, wall.X] = (int)MazeEnum.Cell;
                _disjoint.Join(cell1, cell2);
            }
            _walls.Remove(_walls[wallIndex]);

            if (_disjoint.Size == 1) IsOver = true;

            return Maze;
        }
    }
}
