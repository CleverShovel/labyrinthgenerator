﻿using System;

namespace UltraMaze.Algoritms.MazeGenerators
{
    /// <summary>
    /// Интерфейс для алгоритмов. Все алгоритмы реализуют его.
    /// </summary>
    internal abstract class MazeAlgorithm
    {
        internal bool IsOver = false;
        protected static int Width;
        protected static int Height;
        protected static int[,] Maze;
        protected static Random Randomizer = new Random();
        public abstract int[,] GenerateMaze(int width, int height);
        public abstract int[,] GetNextState();

        public enum MazeEnum : short
        {
            Wall = -2,
            Cell = 0,
            Visited = 1
        };
    }

    internal struct Cell
    {
        public int X;
        public int Y;

        public Cell(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}