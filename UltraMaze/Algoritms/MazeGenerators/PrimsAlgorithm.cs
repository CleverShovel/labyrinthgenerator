﻿using System;
using System.Collections.Generic;

namespace UltraMaze.Algoritms.MazeGenerators
{
    class PrimsAlgorithm : MazeAlgorithm
    {
        private List<Cell> _frontier;

        public override int[,] GenerateMaze(int width, int height)
        {
            // build maze and initialize with only walls
            Height = height;
            Width = width;
            Maze = new int[Height, Width];

            for (var i = 0; i < Height; i++)
                for (var j = 0; j < Width; j++)
                    if ((i % 2 != 0 && j % 2 != 0) && (i < Height - 1 && j < Width - 1))
                        Maze[i, j] = (int)MazeEnum.Cell;
                    else Maze[i, j] = (int)MazeEnum.Wall;

            // select random point and open as start node
            var startCell = new Cell(Randomizer.Next((Width - 1)/2)*2 + 1, Randomizer.Next((Height - 1)/2)*2 + 1);
            Maze[startCell.Y, startCell.X] = (int) MazeEnum.Visited;

            // iterate through direct neighbors of node
            _frontier = new List<Cell>();

            var neighbours = GetNeighbours(Width, Height, Maze, startCell, true);
            for (var i = 0; i < neighbours.Size; i++)
            {
                var neighbourCell = neighbours.Cells[i];
                Maze[neighbourCell.Y, neighbourCell.X] = (int)MazeEnum.Visited + 1;
                _frontier.Add(neighbourCell);
            }

            return Maze;
        }

        private const int Distance = 2;

        private struct CellString
        {
            public Cell[] Cells;
            public int Size;
        }

        public override int[,] GetNextState()
        {
            var currentCell = _frontier[Randomizer.Next(_frontier.Count)];
            _frontier.Remove(currentCell);

            var visitedNeighbours = GetNeighbours(Width, Height, Maze, currentCell, false);
            Maze = RemoveWall(currentCell, visitedNeighbours.Cells[Randomizer.Next(visitedNeighbours.Size)], Maze);

            Maze[currentCell.Y, currentCell.X] = (int) MazeEnum.Visited;

            var neighbours = GetNeighbours(Width, Height, Maze, currentCell, true);
            for (var i = 0; i < neighbours.Size; i++)
            {
                var neighbourCell = neighbours.Cells[i];
                Maze[neighbourCell.Y, neighbourCell.X] = (int) MazeEnum.Visited + 1;
                _frontier.Add(neighbourCell);
            }

            IsOver = _frontier.Count == 0;
            return Maze;
        }

        private CellString GetNeighbours(int width, int height, int[,] maze, Cell c, bool b)
        {
            var x = c.X;
            var y = c.Y;
            var up = new Cell(x, y - Distance);
            var rt = new Cell(x + Distance, y);
            var dw = new Cell(x, y + Distance);
            var lt = new Cell(x - Distance, y);
            var d = new[] { up, rt, dw, lt };
            var size = 0;

            CellString cells;
            cells.Cells = new Cell[4];

            for (var i = 0; i < 4; i++)
            {
                //для каждого направления
                if (d[i].X > 0 && d[i].X < width - 1 && d[i].Y > 0 && d[i].Y < height - 1)
                {
                    //если не выходит за границы лабиринта
                    var mazeCellCurrent = maze[d[i].Y, d[i].X];
                    var cellCurrent = d[i];
                    if ((mazeCellCurrent != (int) MazeEnum.Cell || !b) &&
                        (b || mazeCellCurrent != (int) MazeEnum.Visited)) continue;
                    cells.Cells[size] = cellCurrent;
                    size++;
                }
            }
            cells.Size = size;
            return cells;
        }

        private int[,] RemoveWall(Cell first, Cell second, int[,] maze)
        {
            var xDiff = second.X - first.X;
            var yDiff = second.Y - first.Y;
            Cell target;

            var addX = (xDiff != 0) ? (xDiff / Math.Abs(xDiff)) : 0;
            var addY = (yDiff != 0) ? (yDiff / Math.Abs(yDiff)) : 0;

            target.X = first.X + addX; //координаты стенки
            target.Y = first.Y + addY;

            maze[target.Y, target.X] = (int)MazeEnum.Visited;
            return maze;
        }
    }
}
