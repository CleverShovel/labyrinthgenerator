﻿using System.Collections.Generic;
using System.Linq;

namespace UltraMaze.Algoritms.MazeGenerators
{
    internal class EllersMazeAlgorithm: MazeAlgorithm
    {
        private class Cell
        {
            public bool Right, Down;

            public List<Cell> Set;

            public int X, Y;

            public Cell(int a, int b)
            {
                X = b;
                Y = a;
                Right = false;
                Down = true;
                Set = null;
            }
        }
        
        private Cell[] _cur;

        private Cell[] MakeSet(Cell[] row)
        {
            for (var index = 0; index < row.Length;)
            {
                var cell = row[index++];
                if (cell.Set == null)
                {
                    var list = new List<Cell> {cell};
                    cell.Set = list;
                }
            }
            return row;
        }

        private Cell[] MakeRightWalls(Cell[] row)
        {
            for (var i = 1; i < row.Length; i++)
            {
                if (IsContainsInList(row[i - 1].Set, row[i]))
                {
                    row[i - 1].Right = true;
                    continue;
                }
                if (Randomizer.Next(2) > 0)
                    row[i - 1].Right = true;
                else
                    row = Merge(row, i);
            }
            return row;
        }

        private Cell[] Merge(Cell[] row, int i)
        {  //utility function
            var currentList = row[i - 1].Set;
            var nextList = row[i].Set;
            foreach (var j in nextList)
            {
                currentList.Add(j);
                j.Set = currentList;
            }
            return row;
        }

        private static bool IsContainsInList(List<Cell> set, Cell cell)
        {
            //utility function
            return set.Any(i => i == cell);
        }

        private bool IsNotDone(List<Cell> set)
        {
            //utility function
            return set.Aggregate(true, (current, x) => current && x.Down);
        }

        private Cell[] MakeDown(Cell[] row)
        {
            foreach (var t in row)
            {
                foreach (var x in t.Set) x.Down = true;
                while (IsNotDone(t.Set))
                {
                    do
                    {
                        t.Set[(Randomizer.Next(t.Set.Count))].Down = false;
                    } while (Randomizer.Next(1) > 0);
                }
            }
            return row;
        }

        private int _iterator;

        public override int[,] GenerateMaze(int cellsX, int cellsY)
        {
            Width = (cellsX - 1)/2;
            Height = (cellsY - 1)/2;
            Maze = new int[cellsY, cellsX];
            _cur = new Cell[Width];
            //Creating upper and left boundary

            for (var i = 0; i <= 2*Height; i++)
                Maze[i, 0] = Maze[0, i] = Maze[i, 2*Width] = Maze[2*Height, i] = (int)MazeEnum.Wall;
            for (var i = 2; i <= 2*Height; i += 2)
                for (var j = 2; j <= 2*Width; j += 2)
                    Maze[i, j] = (int)MazeEnum.Wall;

            _iterator = 0;

            for (var i = 0; i < Width; i++)
                _cur[i] = new Cell(0, i);

            return Maze;
        }

        public override int[,] GetNextState()
        {
            _cur = MakeSet(_cur);
            _cur = MakeRightWalls(_cur);
            _cur = MakeDown(_cur);
            if (_iterator == Height - 1)
                _cur = End(_cur);
            PrintMaze(_cur, _iterator);
            if (_iterator != Height - 1)
                _cur = GenNextRow(_cur);
            _iterator++;
            if (_iterator >= Height) IsOver = true;
            return Maze;
        }

        private Cell[] End(Cell[] row)
        {
            for (var i = 1; i < row.Length; i++)
            {
                if (FindPos(row[i - 1].Set, row[i]) == -1)
                {
                    row[i - 1].Right = false;
                    row = Merge(row, i);
                }
            }
            return row;

        }

        private int FindPos(List<Cell> set, Cell x)
        {
            var tmpArray = new Cell[set.Count];
            tmpArray = set.ToArray();
            for (var i = 0; i < tmpArray.Length; i++)
                if (tmpArray[i] == x)
                    return i;
            return -1;
        }

        private Cell[] GenNextRow(Cell[] pre)
        {
            foreach (var t in pre)
            {
                t.Right = false;
                t.X++;
                if (t.Down)
                {
                    t.Set.Remove(t);
                    t.Set = null;
                    t.Down = false;
                }
            }
            return pre;
        }

        static void PrintMaze(Cell[] row, int rowPos)
        {
            rowPos = 2*rowPos + 1;
            for (var i = 0; i < row.Length; i++)
            {
                if (row[i].Right)
                    Maze[rowPos, 2*i + 2] = (int)MazeEnum.Wall;
                if (row[i].Down)
                    Maze[rowPos + 1, 2*i + 1] = (int)MazeEnum.Wall;
            }
        }
    }
}

