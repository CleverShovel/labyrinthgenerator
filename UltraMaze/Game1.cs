﻿using System;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ButtonState = Microsoft.Xna.Framework.Input.ButtonState;
using Keys = Microsoft.Xna.Framework.Input.Keys;

namespace UltraMaze
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        public const int UPS = 20; // Updates per second
        public const int FPS = 60;

        private bool isRightKeyPressed;
        private bool isLeftKeyPressed;
        private bool isUpKeyPressed;
        private bool isDownKeyPressed;

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Texture2D _quadTexture;

        private Rectangle _playerPosition = new Rectangle(QuarterCellSize + CellSize, QuarterCellSize + CellSize,
            HalfCellSize, HalfCellSize);
        KeyboardState _keyboardState;

        public const int CellSize = 20; // Cell pixel width/height
        public const int HalfCellSize = CellSize/2;
        public const int QuarterCellSize = CellSize/4;
        public const int CellsX = 41;
        public const int CellsY = 41;

        public static Vector2 ScreenSize;

        public static Texture2D Pixel;

        private Grid _grid;

        private int _direction;

        readonly Control _gameForm;
        private readonly IntPtr _drawSurface;
        private UltraMazeMainForm _parentForm;
        private readonly PictureBox _pictureBox;

        private int _labyrinthChoice;

        public void LabyrinthChosen(int labyrinth)
        {
            _labyrinthChoice = labyrinth;
            _grid = new Grid(labyrinth);
            _playerPosition = new Rectangle(QuarterCellSize + CellSize, QuarterCellSize + CellSize,
                HalfCellSize, HalfCellSize);
        }

        public Game1(IntPtr drawSurface, UltraMazeMainForm parentForm, PictureBox pictureBox, int labyrinthChoice)
        {
            _labyrinthChoice = labyrinthChoice;
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            _drawSurface = drawSurface;
            _parentForm = parentForm;
            _pictureBox = pictureBox;

            // prepare graphics event
            graphics.PreparingDeviceSettings += graphics_PreparingDeviceSettings;

            _gameForm = Control.FromHandle(Window.Handle);
            _gameForm.VisibleChanged += _gameForm_VisibleChanged;

            //Tell the mouse it will be getting it's input through the pictureBox
            Mouse.WindowHandle = drawSurface;

            IsMouseVisible = true;
        }

        private void graphics_PreparingDeviceSettings(object sender, PreparingDeviceSettingsEventArgs e)
        {
            // Finally attach game1's draw ability to the picture box in winforms.
            e.GraphicsDeviceInformation.PresentationParameters.DeviceWindowHandle = _drawSurface;
        }

        private void _gameForm_VisibleChanged(object sender, EventArgs e)
        {
            if (_gameForm.Visible)
                _gameForm.Visible = false;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            _grid = new Grid(_labyrinthChoice);

            ScreenSize = new Vector2(CellsX, CellsY) * CellSize;

            graphics.PreferredBackBufferWidth = (int)ScreenSize.X;
            graphics.PreferredBackBufferHeight = (int)ScreenSize.Y;
            graphics.ApplyChanges();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            Pixel = new Texture2D(spriteBatch.GraphicsDevice, 1, 1);
            Pixel.SetData(new[] { Color.White });

            _quadTexture = Content.Load<Texture2D>("Player");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                Exit();

            _keyboardState = Keyboard.GetState();

            if (!isUpKeyPressed && _keyboardState.IsKeyDown(Keys.Up))
            {
                _direction = 4;
                if (_playerPosition.Y - CellSize > QuarterCellSize) _playerPosition.Y -= CellSize;
                isUpKeyPressed = true;
            }
            if (isUpKeyPressed && _keyboardState.IsKeyUp(Keys.Up))
                isUpKeyPressed = false;
            if (!isDownKeyPressed && _keyboardState.IsKeyDown(Keys.Down))
            {
                _direction = 2;
                if (_playerPosition.Y + 2*CellSize - CellSize*CellsY < QuarterCellSize) _playerPosition.Y += CellSize;
                isDownKeyPressed = true;
            }
            if (isDownKeyPressed && _keyboardState.IsKeyUp(Keys.Down))
                isDownKeyPressed = false;
            if (!isLeftKeyPressed && _keyboardState.IsKeyDown(Keys.Left))
            {
                _direction = 3;
                if (_playerPosition.X - CellSize > QuarterCellSize) _playerPosition.X -= CellSize;
                isLeftKeyPressed = true;
            }
            if (isLeftKeyPressed && _keyboardState.IsKeyUp(Keys.Left))
                isLeftKeyPressed = false;
            if (!isRightKeyPressed && _keyboardState.IsKeyDown(Keys.Right))
            {
                _direction = 1;
                if (_playerPosition.X + 2*CellSize - CellSize*CellsX < QuarterCellSize) _playerPosition.X += CellSize;
                isRightKeyPressed = true;
            }
            if (isRightKeyPressed && _keyboardState.IsKeyUp(Keys.Right))
                isRightKeyPressed = false;

            if (_grid.GetLivingNeighbors(_playerPosition.X / CellSize, _playerPosition.Y / CellSize))
            {
                if (_direction == 1) _playerPosition.X -= CellSize;
                if (_direction == 2) _playerPosition.Y -= CellSize;
                if (_direction == 3) _playerPosition.X += CellSize;
                if (_direction == 4) _playerPosition.Y += CellSize;
            }

            if (_playerPosition.X/CellSize == CellsX - 2 && _playerPosition.Y/CellSize == CellsY - 2)
                LabyrinthChosen(_labyrinthChoice);

            base.Update(gameTime);

            _grid.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);

            spriteBatch.Begin();
            _grid.Draw(spriteBatch);
            spriteBatch.Draw(_quadTexture, _playerPosition, Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }

        public void GridMazePathCalling(int choise)
        {
            _grid.NewPath(choise);
        }
    }
}
