﻿using System;
using System.Windows.Forms;

namespace UltraMaze
{
    public partial class UltraMazeMainForm : Form
    {
        public int LabyrinthChoice;
        public int PathChoise;
        public UltraMazeMainForm()
        {
            InitializeComponent();
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox2.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public Game1 GameEntry;

        private void LabyrinthMainForm_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
            comboBox2.SelectedIndex = 0;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
            GameEntry.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LabyrinthChoice = comboBox1.SelectedIndex;
            GameEntry.LabyrinthChosen(LabyrinthChoice);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (!msg.HWnd.Equals(Handle) &&
                (keyData == Keys.Left || keyData == Keys.Right ||
                keyData == Keys.Up || keyData == Keys.Down))
                return true;
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            GameEntry.GridMazePathCalling(comboBox2.SelectedIndex);
        }
    }
}
