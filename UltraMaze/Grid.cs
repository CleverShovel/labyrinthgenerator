﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using UltraMaze.Algoritms.MazeGenerators;
using UltraMaze.Algoritms.MazeSolvingAlgorithms;

namespace UltraMaze
{
    internal class Grid
    {
        private readonly Cell[,] _cells;
        private Dictionary<int, Color> _colorsDict;
        private readonly MazeAlgorithm _mazeAlgorithm;
        private PathAlgorithm _pathAlgorithm;
        private int[,] _pathMaze;
        private readonly Random _random = new Random();
        private int[,] _stateMaze;

        public Grid(int chosenLabyrinth)
        {
            Size = new Point(Game1.CellsY, Game1.CellsX);

            _cells = new Cell[Size.Y, Size.X];

            switch (chosenLabyrinth)
            {
                case 0:
                    _mazeAlgorithm = new EllersMazeAlgorithm();
                    break;
                case 1:
                    _mazeAlgorithm = new KruskalsMazeAlgorithm();
                    break;
                case 2:
                    _mazeAlgorithm = new PrimsAlgorithm();
                    break;
                case 3:
                    _mazeAlgorithm = new RecursiveBacktracking();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            _stateMaze = _mazeAlgorithm.GenerateMaze(Size.X, Size.Y);

            for (var i = 0; i < Size.Y; i++)
                for (var j = 0; j < Size.X; j++)
                    _cells[j, i] = new Cell(new Point(i, j)) {Unreachable = (_stateMaze[i, j] < 0)};
        }

        public Point Size { get; }

        public void Update(GameTime gameTime)
        {
            var mouseState = Mouse.GetState();

            foreach (var cell in _cells)
                cell.Update(mouseState);

            /*if (Game1.Paused)
				return;*/
        }

        public bool GetLivingNeighbors(int x, int y)
        {
            return _cells[x, y].Unreachable;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (var cell in _cells)
                cell.Draw(spriteBatch);

            if (_pathMaze != null)
                for (var i = 0; i < Size.Y; i++)
                    for (var j = 0; j < Size.X; j++)
                        if (_pathMaze[i, j] > 0)
                            _cells[j, i].DrawWay(spriteBatch, _colorsDict[_pathMaze[i, j]]);

            if (!_mazeAlgorithm.IsOver)
            {
                _stateMaze = _mazeAlgorithm.GetNextState();

                for (var i = 0; i < Size.Y; i++)
                    for (var j = 0; j < Size.X; j++)
                        _cells[j, i] = new Cell(new Point(j, i)) {Unreachable = (_stateMaze[i, j] < 0)};
            }

            if (_pathAlgorithm == null || _pathAlgorithm.IsOver) return;
            _pathMaze = _pathAlgorithm.GetNextState();
            for (var i = 0; i < Size.Y; i++)
                for (var j = 0; j < Size.X; j++)
                {
                    var t = _pathMaze[i, j];
                    if (t > 0 && !_colorsDict.ContainsKey(t))
                    {
                        var color = new Color(_random.Next(50, 151), _random.Next(50, 151), _random.Next(50, 151),
                            _random.Next(150, 201));
                        if (!_colorsDict.ContainsValue(color)) _colorsDict.Add(t, color);
                        else j--;
                    }
                }
        }

        public void NewPath(int choise)
        {
            if (!_mazeAlgorithm.IsOver) return;

            switch (choise)
            {
                case 0:
                    _pathAlgorithm = new LeeAlgorithm();
                    break;
                case 1:
                    _pathAlgorithm = new RecursiveBacktrackingPath();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            _pathMaze = new int[Size.Y, Size.X];
            _pathMaze = _pathAlgorithm.GeneratePath(_stateMaze);
            _colorsDict = new Dictionary<int, Color>();
            _colorsDict.Add(1,Color.Gold);
        }
    }
}