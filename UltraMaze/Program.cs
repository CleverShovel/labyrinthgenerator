﻿using System;
using System.Windows.Forms;

namespace UltraMaze
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                var form = new UltraMazeMainForm();
            form.Show();
            form.GameEntry = new Game1(form.pictureBox1.Handle, form, form.pictureBox1, form.LabyrinthChoice);
            form.GameEntry.Run();
            }

            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
    }
#endif
}
