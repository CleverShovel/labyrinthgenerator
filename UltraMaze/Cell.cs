﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace UltraMaze
{
	public struct Cell
	{
		public Point Position { get; }
		public Rectangle Bounds { get; }

		public bool Unreachable { get; set; }

		public Cell(Point position)
		{
			Position = position;
			Bounds = new Rectangle(Position.X * Game1.CellSize, Position.Y * Game1.CellSize, Game1.CellSize, Game1.CellSize);

			Unreachable = false;
		}

		public void Update(MouseState mouseState)
		{
			if (Bounds.Contains(new Point(mouseState.X, mouseState.Y)))
			{
				// Your action
			}
		}

		public void Draw(SpriteBatch spriteBatch)
		{
			if (Unreachable)
				spriteBatch.Draw(Game1.Pixel, Bounds, Color.Black);
		}

        public void DrawWay(SpriteBatch spriteBatch, Color color)
        {
            spriteBatch.Draw(Game1.Pixel, Bounds, color);
        }
    }
}
